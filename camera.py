from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, \
    QVBoxLayout, QHBoxLayout, QWidget, QLabel, QMessageBox, QAction, qApp, QDialog, QDialogButtonBox
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot, QTimer, QSize, QRect
from PyQt5.QtGui import QImage, QPixmap, QIcon, QPalette
import numpy as np
from pyqtgraph import ImageView
from cameraModel import *
import os
from seabreeze.spectrometers import Spectrometer
import time
from experimentParams import Ui_Dialog as Form
from cameraParameters import *
from usefuleFuncs import *
import pyqtgraph as pg
import imutils
from kalman_filter import KalmanFilter
import scipy.io as sio
from collections import deque
import pyqtgraph.exporters

rootdir = os.path.dirname(os.path.realpath('__file__'))

outdir = rootdir + '\\' + 'Data_' + time.strftime('%d%m%Y')
if not os.path.isdir(outdir):
    os.makedirs(outdir)

calibration_images_path = rootdir + '\\' + 'calib_images/*.jpg'

lower_green = np.array([40, 40, 40])  # 25, 50, 40
upper_green = np.array([100, 255, 255])

KF = KalmanFilter()


class spectrumThread(QThread):
    drsSignal = pyqtSignal(object, object)
    ignoreSignal = pyqtSignal(object)
    def __init__(self, spectrometer, wavels, speCounter, totSpecs, specWavels, darkNs, whtStd, specRng, opertn, orgn, tiss, pbTp, integTime):
        QThread.__init__(self)
        self.spectrometer = spectrometer
        self.wavels = wavels
        self.speCounter = speCounter
        self.totSpecs = totSpecs
        self.specWavels = specWavels
        self.darkNs = darkNs
        self.whtStd = whtStd
        self.specRng = specRng
        self.opertn = opertn
        self.orgn = orgn
        self.tiss = tiss
        self.pbTp = pbTp
        self.integTime = integTime

    def __del__(self):
        self.wait()

    def run(self):
        while True:
            self.speCounter += 1
            dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
            folder_exp_name = dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_0' + str(
                self.speCounter)
            pathtoexp = outdir + '\\' + folder_exp_name
            if not os.path.isdir(pathtoexp):
                os.makedirs(pathtoexp)
            img_name = pathtoexp + '\\' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.pbTp + '_' + \
                       self.specRng + '_' + 'IntegrationTime' + '_' + str(self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)
            raw_spec = np.zeros((self.wavels.size, self.totSpecs))  # raw spectra
            drs_spec = np.zeros((self.wavels.size, self.totSpecs))  # drs/normalized spectra
            meandrs = np.zeros((self.wavels.size, 1))  # mean drs
            for i in range(0, self.totSpecs):
                spectralData = self.spectrometer.intensities(correct_dark_counts=True, correct_nonlinearity=True)
                specData = np.interp(self.wavels, self.specWavels, spectralData)
                raw_spec[:, i] = specData
                drs = (specData.reshape(self.wavels.size, 1) - self.darkNs) / (self.whtStd - self.darkNs)
                drs_spec[:, i] = drs.reshape(self.wavels.size)
            meandrs = np.mean(drs_spec, axis=1)
            if self.specRng == 'VIS':
                meanIntensity = np.mean(meandrs[100:])  # 420-720
            else:
                meanIntensity = np.mean(meandrs[62:1333])  # 450-1100
            if meanIntensity < 0.001:
                ignoreSpectrum = 1
                ignore = 'ignore'
            else:
                ignoreSpectrum = 0
                ignore = ''
            sio.savemat(img_name + '_ ' + ignore + '.mat',
                        {'wavelength': self.wavels, 'drsSpectra': drs_spec, 'meansDrs': meandrs})
            self.drsSignal.emit(meandrs, self.speCounter)


class StartWindow(QMainWindow):
    def __init__(self, camera=None):
        super().__init__()

        self.setCamPrms = None
        self.setExpPrms = None

        self.camera = camera
        self.timer = QTimer()
        self.timer.timeout.connect(self.nextFrameSlot)
        self.brightness = 80
        self.focus = 15
        self.fps = 30
        self.spec = None
        pixmap = QPixmap('black.jpg')
        self.tracking = None
        self.acquisition = None
        self.countspec = 0
        self.totalSpectra = 20
        self.pts = deque()
        # self.probePos = None
        self.isWorking = False

        self.video_frame = QLabel()
        self.video_frame.setFixedSize(640, 480)
        self.video_frame.setPixmap(pixmap)

        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setFixedSize(640, 480)
        self.graphWidget.hideButtons()
        self.graphWidget.setTitle("Acquired Spectrum", color='#FF0000', size="16px")
        styles = {'color': '#FF0000', 'font-size': '16px'}
        self.graphWidget.setLabel('left', 'Reflectance', units='a.u.', **styles)
        self.graphWidget.setLabel('bottom', 'Wavelength', units='nm', **styles)
        # self.graphWidget.setXRange(420, 1000, padding=0)
        # self.graphWidget.setYRange(0, 1.2, padding=0)
        self.graphWidget.showGrid(x=True, y=True)
        pen = pg.mkPen(color=(255, 0, 0), width=3)
        self.line_ref = self.graphWidget.plot(pen=pen, symbol='o', symbolSize=5, symbolBrush=('b'))
        self.exporter = pg.exporters.ImageExporter(self.graphWidget.plotItem)
        self.exporter.parameters()['width'] = 640

        self.centralWidget = QWidget(self)
        self.centralWidget.resize(1366, 768)
        layout = QGridLayout()
        self.centralWidget.setLayout(layout)
        layout.addWidget(self.video_frame, 0, 0)
        layout.addWidget(self.graphWidget, 0, 1)
        # grid.addWidget(self.video_frame, 2, 2)
        # self.layout = QHBoxLayout(self.centralWidget)
        # self.layout.addWidget(self.video_frame)
        # # self.layout.addWidget(self.video_frame2)
        # self.layout.addWidget(self.view)
        self.setCentralWidget(self.centralWidget)
        # self.setLayout(self.layout)

        self.exitAct = QAction(QIcon('exit.png'), 'Exit', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.exitAct.setStatusTip('Exit Application')
        self.exitAct.triggered.connect(self.close)

        self.startCamera = QAction(QIcon('startVideo.png'), 'Start Camera', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.startCamera.setStatusTip('Start Camera')
        self.startCamera.triggered.connect(self.camera_init)
        self.startCamera.setDisabled(True)

        self.stopCamera = QAction(QIcon('stopVideo.png'), 'Stop Camera', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stopCamera.setStatusTip('Close Camera')
        self.stopCamera.triggered.connect(self.camera_close)
        self.stopCamera.setDisabled(True)

        self.capture = QAction(QIcon('capture.png'), 'Capture Image', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.capture.setStatusTip('Capture Image')
        self.capture.triggered.connect(self.capture_image)
        self.capture.setDisabled(True)

        self.setCamParams = QAction(QIcon('camParams.png'), 'Camera Parameters', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.setCamParams.setStatusTip('Set Camera Parameters')
        self.setCamParams.triggered.connect(self.setCamera)
        self.setCamParams.setDisabled(True)

        self.setExpParams = QAction(QIcon('parameters.png'), 'Experiment Parameters', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.setExpParams.setStatusTip('Set Experiment Parameters')
        self.setExpParams.triggered.connect(self.setExperiment)
        # self.setExpParams.setDisabled(True)

        self.iniTracking = QAction(QIcon('startTracking.png'), 'Track', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.iniTracking.setStatusTip('Start Tracking DRS Probe')
        self.iniTracking.triggered.connect(self.startTracking)
        self.iniTracking.setDisabled(True)

        self.stpTracking = QAction(QIcon('stopTracking.png'), 'No Track', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stpTracking.setStatusTip('Stop Tracking DRS Probe')
        self.stpTracking.triggered.connect(self.stopTracking)
        self.stpTracking.setDisabled(True)

        self.initAcquisition = QAction(QIcon('startSpectrum.png'), 'Acquire Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.initAcquisition.setStatusTip('Start Spectrum Acquisition')
        self.initAcquisition.triggered.connect(self.startAcquisition)
        self.initAcquisition.setDisabled(True)

        self.stpAcquisition = QAction(QIcon('stopSpectrum.png'), 'No Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stpAcquisition.setStatusTip('Stop Spectrum Acquisition')
        self.stpAcquisition.triggered.connect(self.stopAcquisition)
        self.stpAcquisition.setDisabled(True)

        # menubar = self.menuBar()
        # cameraMenu = menubar.addMenu('&Camera')
        # # cameraMenu.addSeparator()
        # cameraMenu.addAction(closeCam)

        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(self.exitAct)
        toolbar.addAction(self.setExpParams)
        toolbar.addSeparator()
        toolbar.addAction(self.startCamera)
        # toolbar.addSeparator()
        toolbar.addAction(self.stopCamera)
        # toolbar.addSeparator()
        toolbar.addAction(self.capture)
        # toolbar.addSeparator()
        toolbar.addAction(self.setCamParams)
        toolbar.addSeparator()
        toolbar.addAction(self.iniTracking)
        toolbar.addAction(self.stpTracking)
        toolbar.addSeparator()

        toolbar.addAction(self.initAcquisition)
        toolbar.addAction(self.stpAcquisition)
        toolbar.setIconSize(QSize(50, 50))
        toolbar.setStyleSheet("QToolBar{spacing:10px;}")

        self.statusBar().showMessage('Ready')
        self.setGeometry(0, 0, 1366, 768)
        self.setWindowTitle('DRS Imaging System')
        self.setWindowIcon(QIcon('icon02.jpg'))
        self.showMaximized()

    def camera_init(self):
        self.cap = self.camera.start_camera()
        if not self.cap.isOpened():
            msgBox = QMessageBox()
            msgBox.setIcon(QMessageBox.Critical)
            msgBox.setWindowTitle("Failure")
            msgBox.setText("Failed to open camera.")
            msgBox.exec_()
            return
        self.camera.set_camera_parameters(self.brightness, self.focus, self.fps)
        self.out_video = self.camera.write_video(rootdir, 'output_raw')
        self.out_video_proc = self.camera.write_video(rootdir, 'output_processed')
        self.spec = Spectrometer.from_first_available()
        self.spec.integration_time_micros(self.integration_time*0.8)
        self.wavelengths = self.spec.wavelengths()
        self.timer.start(1000 / self.fps)
        self.startCamera.setDisabled(True)
        self.stopCamera.setDisabled(False)
        self.capture.setDisabled(False)
        self.setCamParams.setDisabled(False)

    def getSpectrum(self):
        self.countspec += 1
        dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
        folder_exp_name = dateexp + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_0' + str(self.countspec)
        pathtoexp = outdir + '\\' + folder_exp_name
        if not os.path.isdir(pathtoexp):
            os.makedirs(pathtoexp)
        raw_spec = np.zeros((self.myRGB_WLx.size, self.totalSpectra))  # raw spectra
        drs_spec = np.zeros((self.myRGB_WLx.size, self.totalSpectra))  # drs/normalized spectra
        self.meandrs = np.zeros((self.myRGB_WLx.size, 1))  # mean drs
        for i in range(0, self.totalSpectra):
            spectralData = self.spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)
            specData = np.interp(self.myRGB_WLx, self.wavelengths, spectralData)
            raw_spec[:, i] = specData
            drs = (specData.reshape(self.myRGB_WLx.size, 1) - self.darknoise) / (self.whitestd - self.darknoise)
            drs_spec[:, i] = drs.reshape(self.myRGB_WLx.size)
        self.meandrs = np.mean(drs_spec, axis=1)
        if self.spectral_range == 'VIS':
            meanIntensity = np.mean(self.meandrs[100:])  # 420-720
        else:
            meanIntensity = np.mean(self.meandrs[62:1333])  # 450-1100
        if meanIntensity < 0.001:
            self.ignoreSpectrum = 1
            ignore = 'ignore'
        else:
            self.ignoreSpectrum = 0
            ignore = ''
        self.img_name = pathtoexp + '\\' + dateexp + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_' + self.probe_type + '_' + self.spectral_range + '_' + 'IntegrationTime' + '_' + str(self.integration_time*0.8) + '_' + 'Position' + '_0' + str(self.countspec) + '_ ' + ignore
        sio.savemat(self.img_name + '.mat', {'wavelength': self.myRGB_WLx, 'drsSpectra': drs_spec, 'meansDrs': self.meandrs})

    def loadCalibration(self):
        if self.spectral_range == 'VIS':
            self.myRGB_WLx = np.arange(start=420, stop=720.5, step=0.5)
            darknoise = sio.loadmat('darknoiseVIS.mat')
            self.darknoise = darknoise['darknoise']
            whitestd = sio.loadmat('whitestdVIS.mat')
            self.whitestd = whitestd['whitestd']
            self.graphWidget.setXRange(400, 750, padding=0)
            self.graphWidget.setYRange(0, 1.2, padding=0)
        elif self.spectral_range == 'VNIR':
            self.myRGB_WLx = np.arange(start=450, stop=1100, step=0.33829)
            darknoise = sio.loadmat('darknoiseVNIR.mat')
            self.darknoise = darknoise['darknoise']
            whitestd = sio.loadmat('whitestdVNIR.mat')
            self.whitestd = whitestd['whitestd']
            self.graphWidget.setXRange(400, 1200, padding=0)
            self.graphWidget.setYRange(0, 1.2, padding=0)
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Success")
        msg.setText("Calibration data for {} spectral range successfully loaded.".format(self.spectral_range))
        msg.exec_()
        return

    def nextFrameSlot(self):
        ret, self.frame = self.camera.get_frame()
        # self.frame = cv2.flip(self.frame, 1)
        capFrame = self.frame
        self.out_video.write(capFrame)
        frm = self.frame
        if ret:
            if self.tracking == 1:
                gray = cv2.cvtColor(frm, cv2.COLOR_BGR2GRAY)
                aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
                parameters = aruco.DetectorParameters_create()
                parameters.adaptiveThreshConstant = 10
                corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

                blurred = cv2.GaussianBlur(frm, (7, 7), 0)
                hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
                mask = cv2.inRange(hsv, lower_green, upper_green)
                mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, (6, 6))
                mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, (20, 20))
                mask = cv2.GaussianBlur(mask, (3, 3), 0)
                cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                cnts = imutils.grab_contours(cnts)

                centre = None
                if len(cnts) > 0:
                    c = max(cnts, key=cv2.contourArea)
                    angle_pca = getOrientation(c)
                    M = cv2.moments(c)
                    if M["m00"] != 0:
                        centre = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                    else:
                        centre = (0, 0)
                    rect = cv2.minAreaRect(c)
                    box = np.int0(cv2.boxPoints(rect))
                    box_ = order_points(box)
                    (tl, tr, br, bl) = box_
                    (tltrX, tltrY) = midpoint(tl, tr)
                    (blbrX, blbrY) = midpoint(bl, br)
                    (tlblX, tlblY) = midpoint(tl, bl)
                    (trbrX, trbrY) = midpoint(tr, br)
                    tltr = [int(tltrX), int(tltrY)]
                    blbr = [int(blbrX), int(blbrY)]
                    tlbl = [int(tlblX), int(tlblY)]
                    trbr = [int(trbrX), int(trbrY)]
                    l1 = makeLine(tltr, blbr)
                    l2 = makeLine(tlbl, trbr)
                    center = intersection(l1, l2)
                    center = (int(center[0]), int(center[1]))

                    change_point = (0, 0)
                    if corners != []:
                        artl = (int(corners[0][0][0][0]), int(corners[0][0][0][1]))
                        artr = (int(corners[0][0][1][0]), int(corners[0][0][1][1]))
                        arbr = (int(corners[0][0][2][0]), int(corners[0][0][2][1]))
                        arbl = (int(corners[0][0][3][0]), int(corners[0][0][3][1]))
                        cv2.circle(frm, (int(corners[0][0][0][0]), int(corners[0][0][0][1])), 5, (0, 0, 0),
                                   -1)  # upper-left
                        cv2.circle(frm, (int(corners[0][0][1][0]), int(corners[0][0][1][1])), 5, (0, 255, 0),
                                   -1)  # upper-right
                        cv2.circle(frm, (int(corners[0][0][2][0]), int(corners[0][0][2][1])), 5, (0, 0, 255),
                                   -1)  # bottom-right
                        cv2.circle(frm, (int(corners[0][0][3][0]), int(corners[0][0][3][1])), 5, (255, 0, 0),
                                   -1)  # bottom-left
                        (artltrX, artltrY) = midpoint(artl, artr)
                        (arblbrX, arblbrY) = midpoint(arbl, arbr)
                        (artlblX, artlblY) = midpoint(artl, arbl)
                        (artrbrX, artrbrY) = midpoint(artr, arbr)
                        artltr = [int(artltrX), int(artltrY)]
                        arblbr = [int(arblbrX), int(arblbrY)]
                        artlbl = [int(artlblX), int(artlblY)]
                        artrbr = [int(artrbrX), int(artrbrY)]
                        arl1 = makeLine(artltr, arblbr)
                        arl2 = makeLine(artlbl, artrbr)
                        centre_aruco = intersection(arl1, arl2)
                        centre_aruco = (int(centre_aruco[0]), int(centre_aruco[1]))
                        cv2.circle(frm, centre_aruco, 5, (0, 255, 0), -1)
                        if int(artltrX) == int(arblbrX):
                            start_pointX = int(artltrX)
                            start_pointY = 0
                            end_pointX = int(artltrX)
                            end_pointY = 480
                            angle_aruco = 0
                        else:
                            angle_aruco = slope(int(artltrX), int(artltrY), int(arblbrX), int(arblbrY))
                            start_pointX = 0
                            start_pointY = int(-(int(artltrX) - 0) * angle_aruco + int(artltrY))
                            end_pointX = 640
                            end_pointY = int(-(int(arblbrX) - 640) * angle_aruco + int(arblbrY))
                        cv2.line(frm, (start_pointX, start_pointY), (end_pointX, end_pointY), (0, 128, 128), 2, 8)

                        if start_pointX != None and start_pointY != None and end_pointX != None and end_pointY != None:
                            line_vals = createLineIterator((start_pointX, start_pointY), (end_pointX, end_pointY), mask)
                            line_vals_sorted = line_vals[line_vals[:, 1].argsort()[::-1]]

                            if angle_aruco >= 0:
                                for i in range(1, len(line_vals_sorted)):
                                    if line_vals_sorted[i, 1] < line_vals_sorted[i - 1, 1]:
                                        if 0 < line_vals_sorted[i, 2] <= 255:
                                            change_point = line_vals_sorted[i - 1]
                                            change_point = (int(change_point[0]), int(change_point[1]))
                                            break
                            elif angle_aruco < 0:
                                for i in range(1, len(line_vals)):
                                    if line_vals[i, 1] < line_vals[i - 1, 1]:
                                        if 0 < line_vals[i, 2] <= 255:
                                            change_point = line_vals[i - 1]  # i+1
                                            change_point = (int(change_point[0]), int(change_point[1]))
                                            break

                    else:
                        if angle_pca < 0.0:
                            line_vals = createLineIterator(center, (int(tlblX) - 5, int(tlblY) + 5), mask)
                            for i in range(1, len(line_vals)):
                                if line_vals[i, 2] <= 255 and line_vals[i][1] >= tlblY:
                                    if line_vals[i, 2] == 0:
                                        change_point = line_vals[i - 1]
                                    else:
                                        change_point = line_vals[i]
                                    change_point = (int(change_point[0]), int(change_point[1]))
                                    break
                                else:
                                    change_point = (int(tlblX), int(tlblY))
                                    break
                        elif 0.0 < angle_pca < 0.8:
                            line_vals = createLineIterator(center, (int(trbrX) + 5, int(trbrY) + 5), mask)
                            for i in range(1, len(line_vals)):
                                if line_vals[i, 2] <= 255 and line_vals[i][0] >= trbrX:
                                    if line_vals[i, 2] == 0:
                                        change_point = line_vals[i - 1]
                                    else:
                                        change_point = line_vals[i]
                                    change_point = (int(change_point[0]), int(change_point[1]))
                                    break
                                else:
                                    change_point = (int(trbrX), int(trbrY))
                                    break
                        else:
                            line_vals = createLineIterator(centre, (int(blbrX), int(blbrY) + 20), mask)
                            for i in range(1, len(line_vals)):
                                if line_vals[i, 2] == 0:
                                    change_point = line_vals[i - 1]
                                    change_point = (int(change_point[0]), int(change_point[1]))
                                    break
                                else:
                                    change_point = (int(blbrX), int(blbrY))
                                    break

                    pred = KF.predict()
                    kfPt = KF.correct(change_point, 1)
                    self.change_point = (int(kfPt[0][0]), int(kfPt[0][1]))

                    cv2.circle(frm, change_point, 3, (255, 0, 0), -1)

                    if self.acquisition == 1:
                        self.specThread.start()
                        if self.isWorking == True:
                            # if self.ignoreSpectrum == 0:
                            # self.probePos = self.change_point
                            prPs = (self.change_point[0], self.change_point[1])  # - 3)
                            self.pts.appendleft(prPs)
                            # f = open(outdir + '\\' + time.strftime("%d%m%Y") +'_' + self.organ + '_' + self.tissue + '_ProbePosition.txt', 'a')
                            # self.f.write(str(countspec) + '.' + '' + str(change_point) + '\n')
                            # f.close()
                            # cv2.ellipse(self.capturedImage, (probePos[0]+3, probePos[1]-2), (1, 1), 0, 0, 360, (0, 0, 255), -1)
                            # cv2.putText(self.capturedImage, str(self.countspec), prPs, cv2.FONT_HERSHEY_DUPLEX, 0.2, (255, 255, 255), 1)
                            # img_name = self.image_path + '\\' + self.date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_Annotated.png'
                            # cv2.imwrite(img_name, self.capturedImage)
                            # self.graphWidget.setTitle("Acquired Spectrum {}".format(self.countspec), color='#FF0000', size="16px")
                            # self.line_ref.setData(self.myRGB_WLx, self.meandrs)
                            # self.exporter.export(self.img_name + '.png')
                            # self.countspec += 1
                            self.isWorking = False
            for i in range(0, len(self.pts)):
                if self.pts[i] is None:
                    continue
                cv2.circle(frm, self.pts[i], 3, (0, 255, 0), -1)
            self.out_video_proc.write(frm)
            frm = cv2.cvtColor(self.frame, cv2.COLOR_BGR2RGB)
            image = QImage(frm, frm.shape[1], frm.shape[0], QImage.Format_RGB888)
            pixmap = QPixmap.fromImage(image)
            self.video_frame.setPixmap(pixmap)

    def plotSpectrum(self, spectrum, countspec):
        self.isWorking = True
        self.graphWidget.setTitle("Acquired Spectrum {}".format(countspec), color='#FF0000', size="16px")
        self.line_ref.setData(self.myRGB_WLx, spectrum)

    def saveProbePosition(self, spectrum, countspec):
        # f = open(outdir + '\\' + time.strftime("%d%m%Y") + '_' + self.organ + '_' + self.tissue + '_ProbePosition.txt', 'a')
        self.f.write(str(countspec) + '.' + '' + str(self.change_point) + '\n')
        # f.close()
        cv2.ellipse(self.capturedImage, (self.change_point[0] + 3, self.change_point[1] - 2), (1, 1), 0, 0, 360, (0, 0, 255), -1)
        # cv2.putText(self.capturedImage, str(self.countspec), prPs, cv2.FONT_HERSHEY_DUPLEX, 0.2, (255, 255, 255), 1)
        img_name = self.image_path + '\\' + self.date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_Annotated.png'
        cv2.imwrite(img_name, self.capturedImage)

    def startTracking(self):
        self.tracking = 1
        self.iniTracking.setDisabled(True)
        self.stpTracking.setDisabled(False)

    def stopTracking(self):
        self.tracking = 0
        self.iniTracking.setDisabled(False)
        self.stpTracking.setDisabled(True)

    def startAcquisition(self):
        self.acquisition = 1
        self.f = open(outdir + '\\' + time.strftime("%d%m%Y") + '_' + self.organ + '_' + self.tissue + '_ProbePosition.txt',
                 'a')
        self.initAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(False)
        self.specThread = spectrumThread(self.spec, self.myRGB_WLx, self.countspec, self.totalSpectra, self.wavelengths, self.darknoise,
                                         self.whitestd, self.spectral_range, self.operation, self.organ, self.tissue,
                                         self.probe_type, self.integration_time)
        self.specThread.drsSignal.connect(self.plotSpectrum)
        self.specThread.drsSignal.connect(self.saveProbePosition)

    def stopAcquisition(self):
        self.acquisition = 0
        self.f.close()
        # self.graphWidget.clear()
        self.graphWidget.setTitle("Acquired Spectrum", color='#FF0000', size="16px")
        self.initAcquisition.setDisabled(False)
        self.stpAcquisition.setDisabled(True)
        self.specThread.terminate()

    def camera_close(self):
        self.camera.stop_camera()
        self.timer.stop()
        pixmap = QPixmap('black.jpg')
        self.video_frame.setPixmap(pixmap)
        self.startCamera.setDisabled(False)
        self.setCamParams.setDisabled(True)
        self.capture.setDisabled(True)
        self.stopCamera.setDisabled(True)
        self.initAcquisition.setDisabled(True)
        self.iniTracking.setDisabled(True)
        self.stpAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(True)
        self.acquisition = 0
        self.tracking = 0

    def capture_image(self):
        self.date_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.image_path = outdir
        image_name = self.image_path + '\\' + self.date_time + '.png'
        success = cv2.imwrite(image_name, self.frame)
        self.capturedImage = self.frame
        qtimage = QImage(self.frame, self.frame.shape[1], self.frame.shape[0], QImage.Format_RGB888)
        self.pixmap_image = QPixmap.fromImage(qtimage)
        if success:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Image captured and saved in \n {}.".format(self.image_path))
            msg.exec_()
            self.iniTracking.setDisabled(False)
            self.initAcquisition.setDisabled(False)
            return

    def setCamera(self):
        self.setCamPrms = Example()
        self.setCamPrms.show()
        self.setCamPrms.ok_button.clicked.connect(self.getCameraParams)

    def getCameraParams(self):
        self.brightness = self.setCamPrms.brightness_sld.value()
        self.focus = self.setCamPrms.focus_sld.value()
        self.camera.set_camera_parameters(self.brightness, self.focus, self.fps)
        if self.brightness == 0 or self.focus == 0:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Camera brightness and/or focus value(s) cannot be 0.")
            msg.exec_()
            return
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Parameters successfully passed.")
            msg.exec_()
            self.setCamPrms.close_button.setDisabled(False)
            return

    def setExperiment(self):
        self.dialog = QDialog()
        self.dialog.ui = Form()
        self.dialog.ui.setupUi(self.dialog)
        self.dialog.show()
        self.dialog.ui.getParams_pushbutton.clicked.connect(self.getExperimentParams)

    def getExperimentParams(self):
        self.operation = self.dialog.ui.operation_lineEdit.text()
        self.organ = self.dialog.ui.organ_lineEdit.text()
        self.probe_type = str(self.dialog.ui.probe_type_comboBox.currentText())
        self.tissue = self.dialog.ui.tissue_lineEdit.text()
        self.calibration = str(self.dialog.ui.calibration_comboBox.currentText())
        self.integration_time = self.dialog.ui.integration_time_lineEdit.text()
        self.spectral_range = str(self.dialog.ui.spectral_range_comboBox.currentText())
        if self.operation == "" or self.organ == "" or self.probe_type == "" or self.tissue == "" or self.calibration == "" or self.integration_time == "" or self.spectral_range == "":
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Please fill in all parameters.")
            msg.exec_()
            return
        elif is_integer(self.operation) or is_integer(self.organ) or is_integer(self.tissue):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Operation/Organ/Tissue name must be string not number.")
            msg.exec_()
            return
        elif not is_integer(self.integration_time):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Spectrometer's integration time must be number not string.")
            msg.exec_()
            return
        else:
            self.integration_time = int(self.integration_time)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Parameters successfully passed.")
            msg.exec_()
            self.dialog.ui.exp_params_buttonBox.button(QDialogButtonBox.Ok).setDisabled(False)
            if self.dialog.exec_() == QDialog.Accepted:
                self.startCamera.setDisabled(False)
                self.loadCalibration()
            return


if __name__ == '__main__':
    app = QApplication([])
    window = StartWindow(Camera(1))  # You should delete the Camera(0) in order to use it in the main.py
    window.show()
    app.exit(app.exec_())
