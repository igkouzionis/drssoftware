# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


from PyInstaller.utils.hooks import collect_submodules

hidden_imports = collect_submodules('sklearn')

a = Analysis(['gui_02.py'],
             pathex=['C:\\Users\\ioannis\\PycharmProjects\\drs_v5_6'],
             binaries=[],
             datas=[('icon02.jpg', '.'), ('black.jpg', '.'), ('camParams.png', '.'), ('capture.png', '.'), ('exit.png', '.'), ('parameters.png', '.'), ('pause.png', '.'), ('play.png', '.'), ('calibration.png', '.'), ('startSpectrum.png', '.'), ('startTracking.png', '.'), ('startVideo.png', '.'), ('stopCamera.png', '.'), ('stopSpectrum.png', '.'), ('stopTracking.png', '.'), ('stopVideo.png', '.'), ('calib_images/*.jpg', 'calibration_images'),
             ('lightON.png', '.'), ('lightOFF.png', '.'), ('oesophagusClassifier.pkl', '.'), ('std_scaler_oesophagus.pkl', '.'), ('std_scaler_stomach.pkl', '.'), ('stomachClassifier.pkl', '.'), ('calib_standard_probe/*.mat', 'calib_standard_probe'), ('calib_sterile_probe/*.mat', 'calib_sterile_probe')],
             hiddenimports=hidden_imports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='DRS App',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True,
          uac_admin=True,
          icon='drs.ico' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='gui_02')
