'''
@Author: igkouzionis
@Time: 20/11/2020 11:25
@File: recoverAnnotatedImg.py
@Software: PyCharm
'''

import cv2
import numpy as np
from matplotlib import pyplot as plt
from scipy.io import loadmat
import os

# Load original captured image
origImg = cv2.imread(r'C:\Users\User\PycharmProjects\drs_v4_9_qt\Data_17112020\Data_17112020_a\2020-11-17_17-10-17.png')
# origImg = cv2.cvtColor(origImg, cv2.COLOR_BGR2RGB)
# plt.figure(1)
# plt.imshow(origImg)
# plt.show()

# Load probe's positions
pb = loadmat(r'C:\Users\User\PycharmProjects\drs_v4_9_qt\Data_17112020\Data_17112020_a\probePose.mat')
probePos = pb['probePos']
# print(probePos)

# Draw probe locations on tissue original image
for i in range(0, len(probePos)):
    cv2.ellipse(origImg, (probePos[i][0] + 3, probePos[i][1] - 2), (1, 1), 0, 0, 360, (255, 0, 0), -1)
cv2.imwrite(r'normal_out_annotated_17_11_2020.png', origImg)