# README #

This repository contains a part of my research work done during my PhD on Surgical Imaging and Image-guided interventions for cancer diagnosis and margin assessment.

This is the main GUI used for the control of the light source, spectrometer and camera of our system, the acquisition of diffuse reflectance spectra in real-time (~80 spectra/sec), the tracking of the DRS fibre probe, the annotation of the specimen under study in real-time with the probed positions, the real-time processing and classification of the acquired DRS spectra.

More information you can find in the report and presentation (video demos are also available).
Do not hesitate to contact me at ioannis.gkouzionis@gmail.com if you have any further questions.

Ioannis Gkouzionis.