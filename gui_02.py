'''
@Author: igkouzionis
@Time: 04/10/2020 11:16
@File: gui.py
@Software: PyCharm
'''

# from PyQt5 import uic
import json

import cv2
from PyQt5.QtWidgets import QApplication, QMainWindow, QPushButton, \
    QVBoxLayout, QHBoxLayout, QWidget, QLabel, QMessageBox, QAction, qApp, QDialog, QDialogButtonBox, QFileDialog
from PyQt5.QtCore import QThread, Qt, pyqtSignal, pyqtSlot, QTimer, QSize, QRect
from PyQt5.QtGui import QImage, QPixmap, QIcon, QPalette
import numpy as np
from pyqtgraph import ImageView
from cameraModel import *
import os
from seabreeze.spectrometers import Spectrometer, list_devices
import time
from experimentParams import Ui_Dialog as Form
from cameraParameters import *
from usefuleFuncs import *
from calibration import *
import pyqtgraph as pg
import imutils
from kalman_filter import KalmanFilter
import scipy.io as sio
from collections import deque
import pyqtgraph.exporters
import polyROISelector
from shapely import geometry
import joblib
from sklearn.preprocessing import StandardScaler
from scipy.signal import savgol_filter as golayFilter
from matplotlib import cm
import serial
# from KalmanFilter import KalmanFilter


os.environ['OPENCV_VIDEOIO_PRIORITY_MSMF'] = '0'

rootdir = os.path.dirname(os.path.realpath('__file__'))

outdir = rootdir + '\\' + 'Data_' + time.strftime('%d%m%Y')
if not os.path.isdir(outdir):
    os.makedirs(outdir)

calibration_images_path = rootdir + '\\' + 'calib_images/*.jpg'

lower_green = np.array([40, 40, 40])  # 25, 50, 40
upper_green = np.array([100, 255, 255])

KF = KalmanFilter()
# KF = KalmanFilter(0.1, 1, 1, 1, 0.1, 0.1)

class cameraThread(QThread):
########################################################################################
    frameSignal = pyqtSignal(object, object)
    # capSignal = pyqtSignal(object)
    imgSignal = pyqtSignal(object)

    def __init__(self, camNum, brightness, focus, fps, dir_path):
        QThread.__init__(self)
        self.camNum = camNum
        self.brightness = brightness
        self.focus = focus
        self.fps = fps
        # print(self.brightness)
        self.cap = cv2.VideoCapture(self.camNum)  # for HH laptop
        # self.cap = cv2.VideoCapture(cv2.CAP_DSHOW + self.camNum)  # for CXH laptop
        self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, True)
        self.cap.set(cv2.CAP_PROP_AUTOFOCUS, False)
        self.cap.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)
        self.cap.set(cv2.CAP_PROP_FOCUS, self.focus)
        self.cap.set(cv2.CAP_PROP_FPS, self.fps)
        self.roi = None
        self.ROISelector = None
        self.dirpath = dir_path
        time.sleep(0.5)

    def stop(self):
        if self.cap is not None:
            self.cap.release()
            print("[INFO] Stop webcam")

    def run(self):
        global isTracking
        # time.sleep(1)
        while True:
            # cap = self.cap
            # cap = cv2.VideoCapture(self.camNum)
            # cap = cv2.VideoCapture(self.camNum)
            # self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, True)
            # self.cap.set(cv2.CAP_PROP_AUTOFOCUS, False)
            # self.cap.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)
            # self.cap.set(cv2.CAP_PROP_FOCUS, self.focus)
            # self.cap.set(cv2.CAP_PROP_FPS, self.fps)
            # time.sleep(1)
            # while self.cap.isOpened():
            ret, self.frame = self.cap.read()
            self.copyFrame = self.frame.copy()

            if ret:
                if isTracking == 1:
                    gray = cv2.cvtColor(self.frame, cv2.COLOR_BGR2GRAY)
                    aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
                    parameters = aruco.DetectorParameters_create()
                    parameters.adaptiveThreshConstant = 10
                    corners, ids, rejectedImgPoints = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)

                    blurred = cv2.GaussianBlur(self.frame, (7, 7), 0)
                    hsv = cv2.cvtColor(blurred, cv2.COLOR_BGR2HSV)
                    mask = cv2.inRange(hsv, lower_green, upper_green)
                    mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, (6, 6))
                    mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, (20, 20))
                    mask = cv2.GaussianBlur(mask, (3, 3), 0)
                    cnts = cv2.findContours(mask.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
                    cnts = imutils.grab_contours(cnts)

                    centre = None
                    if len(cnts) > 0:
                        c = max(cnts, key=cv2.contourArea)
                        angle_pca = getOrientation(c)
                        M = cv2.moments(c)
                        if M["m00"] != 0:
                            centre = (int(M["m10"] / M["m00"]), int(M["m01"] / M["m00"]))
                        else:
                            centre = (0, 0)
                        rect = cv2.minAreaRect(c)
                        box = np.int0(cv2.boxPoints(rect))
                        box_ = order_points(box)
                        (tl, tr, br, bl) = box_
                        (tltrX, tltrY) = midpoint(tl, tr)
                        (blbrX, blbrY) = midpoint(bl, br)
                        (tlblX, tlblY) = midpoint(tl, bl)
                        (trbrX, trbrY) = midpoint(tr, br)
                        tltr = [int(tltrX), int(tltrY)]
                        blbr = [int(blbrX), int(blbrY)]
                        tlbl = [int(tlblX), int(tlblY)]
                        trbr = [int(trbrX), int(trbrY)]
                        l1 = makeLine(tltr, blbr)
                        l2 = makeLine(tlbl, trbr)
                        center = intersection(l1, l2)
                        center = (int(center[0]), int(center[1]))

                        change_point = (0, 0)
                        if corners != []:
                            artl = (int(corners[0][0][0][0]), int(corners[0][0][0][1]))
                            artr = (int(corners[0][0][1][0]), int(corners[0][0][1][1]))
                            arbr = (int(corners[0][0][2][0]), int(corners[0][0][2][1]))
                            arbl = (int(corners[0][0][3][0]), int(corners[0][0][3][1]))
                            cv2.circle(self.frame, (int(corners[0][0][0][0]), int(corners[0][0][0][1])), 5, (0, 0, 0),
                                       -1)  # upper-left
                            cv2.circle(self.frame, (int(corners[0][0][1][0]), int(corners[0][0][1][1])), 5, (0, 255, 0),
                                       -1)  # upper-right
                            cv2.circle(self.frame, (int(corners[0][0][2][0]), int(corners[0][0][2][1])), 5, (0, 0, 255),
                                       -1)  # bottom-right
                            cv2.circle(self.frame, (int(corners[0][0][3][0]), int(corners[0][0][3][1])), 5, (255, 0, 0),
                                       -1)  # bottom-left
                            (artltrX, artltrY) = midpoint(artl, artr)
                            (arblbrX, arblbrY) = midpoint(arbl, arbr)
                            (artlblX, artlblY) = midpoint(artl, arbl)
                            (artrbrX, artrbrY) = midpoint(artr, arbr)
                            artltr = [int(artltrX), int(artltrY)]
                            arblbr = [int(arblbrX), int(arblbrY)]
                            artlbl = [int(artlblX), int(artlblY)]
                            artrbr = [int(artrbrX), int(artrbrY)]
                            arl1 = makeLine(artltr, arblbr)
                            arl2 = makeLine(artlbl, artrbr)
                            centre_aruco = intersection(arl1, arl2)
                            centre_aruco = (int(centre_aruco[0]), int(centre_aruco[1]))
                            cv2.circle(self.frame, centre_aruco, 5, (0, 255, 0), -1)
                            if int(artltrX) == int(arblbrX):
                                start_pointX = int(artltrX)
                                start_pointY = 0
                                end_pointX = int(artltrX)
                                end_pointY = 480
                                angle_aruco = 0
                            else:
                                angle_aruco = slope(int(artltrX), int(artltrY), int(arblbrX), int(arblbrY))
                                start_pointX = 0
                                start_pointY = int(-(int(artltrX) - 0) * angle_aruco + int(artltrY))
                                end_pointX = 640
                                end_pointY = int(-(int(arblbrX) - 640) * angle_aruco + int(arblbrY))
                            cv2.line(self.frame, (start_pointX, start_pointY), (end_pointX, end_pointY), (0, 128, 128),
                                     2, 8)

                            if start_pointX != None and start_pointY != None and end_pointX != None and end_pointY != None:
                                line_vals = createLineIterator((start_pointX, start_pointY), (end_pointX, end_pointY),
                                                               mask)
                                line_vals_sorted = line_vals[line_vals[:, 1].argsort()[::-1]]

                                if angle_aruco >= 0:
                                    for i in range(1, len(line_vals_sorted)):
                                        if line_vals_sorted[i, 1] < line_vals_sorted[i - 1, 1]:
                                            if 0 < line_vals_sorted[i, 2] <= 255:
                                                change_point = line_vals_sorted[i - 1]
                                                change_point = (int(change_point[0]), int(change_point[1]))
                                                break
                                elif angle_aruco < 0:
                                    for i in range(1, len(line_vals)):
                                        if line_vals[i, 1] < line_vals[i - 1, 1]:
                                            if 0 < line_vals[i, 2] <= 255:
                                                change_point = line_vals[i - 1]  # i+1
                                                change_point = (int(change_point[0]), int(change_point[1]))
                                                break

                        else:
                            if angle_pca < 0.0:
                                line_vals = createLineIterator(center, (int(tlblX) - 5, int(tlblY) + 5), mask)
                                for i in range(1, len(line_vals)):
                                    if line_vals[i, 2] <= 255 and line_vals[i][1] >= tlblY:
                                        if line_vals[i, 2] == 0:
                                            change_point = line_vals[i - 1]
                                        else:
                                            change_point = line_vals[i]
                                        change_point = (int(change_point[0]), int(change_point[1]))
                                        break
                                    else:
                                        change_point = (int(tlblX), int(tlblY))
                                        break
                            elif 0.0 < angle_pca < 0.8:
                                line_vals = createLineIterator(center, (int(trbrX) + 5, int(trbrY) + 5), mask)
                                for i in range(1, len(line_vals)):
                                    if line_vals[i, 2] <= 255 and line_vals[i][0] >= trbrX:
                                        if line_vals[i, 2] == 0:
                                            change_point = line_vals[i - 1]
                                        else:
                                            change_point = line_vals[i]
                                        change_point = (int(change_point[0]), int(change_point[1]))
                                        break
                                    else:
                                        change_point = (int(trbrX), int(trbrY))
                                        break
                            else:
                                line_vals = createLineIterator(centre, (int(blbrX), int(blbrY) + 20), mask)
                                for i in range(1, len(line_vals)):
                                    if line_vals[i, 2] == 0:
                                        change_point = line_vals[i - 1]
                                        change_point = (int(change_point[0]), int(change_point[1]))
                                        break
                                    else:
                                        change_point = (int(blbrX), int(blbrY))
                                        break

                        pred = KF.predict()
                        # (x_pred, y_pred) = KF.predict()
                        kfPt = KF.correct(change_point, 1)
                        # (x_update, y_update) = KF.update(np.array([[change_point[0]], [change_point[1]]]))
                        self.change_point = (int(kfPt[0][0]), int(kfPt[0][1]))
                        # self.change_point = (int(x_update), int(y_update))



                        # CHANGE v5.4
                        # cv2.circle(self.frame, self.change_point, 3, (255, 0, 0), -1)
                        self.frameSignal.emit(self.frame, self.change_point)
                else:
                    self.frameSignal.emit(self.frame, None)
        # self.cap.release()

    def capture(self):
        global capturedImage
        self.date_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.image_path = self.dirpath
        image_name = self.image_path + '/' + self.date_time + '.png'
        capturedImage = self.copyFrame
        success = cv2.imwrite(image_name, capturedImage)
        # qtimage = QImage(self.frame, self.frame.shape[1], self.frame.shape[0], QImage.Format_RGB888)
        # self.pixmap_image = QPixmap.fromImage(qtimage)
        if success:
            # self.imgSignal.emit(self.frame)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Image captured and saved in \n {}.".format(self.image_path))
            msg.exec_()
            # self.roi = cv2.selectROI('Image ROI', capturedImage, False)
            return

    def selectROI(self):
        cv2.imshow("ROI", capturedImage)
        clone = capturedImage.copy()
        clone2 = capturedImage.copy()
        self.ROISelector = polyROISelector.orientedROISelector(clone, windowName="ROI")
        while True:
            k = cv2.waitKey(1)
            if k == ord('r'):
                self.ROISelector.resetCanvas(clone2.copy())
            elif k == (27):
                break
        cv2.destroyAllWindows()


class spectrumThread(QThread):
    # drsSignal = pyqtSignal(object, object)
    drsSignal = pyqtSignal(object, object, object)
    ignoreSignal = pyqtSignal(object)

    def __init__(self, spectrometer, wavels, speCounter, totSpecs, specWavels, darkNs, whtStd, specRng, opertn, orgn,
                 tiss, pbTp, integTime, gender, age, therapy, tissue_wall, change_point, pathtoexp, nts):
        QThread.__init__(self)
        self.spectrometer = spectrometer
        self.wavels = wavels
        self.speCounter = speCounter
        self.totSpecs = totSpecs
        self.specWavels = specWavels
        self.darkNs = darkNs
        self.whtStd = whtStd
        self.specRng = specRng
        self.opertn = opertn
        self.orgn = orgn
        self.tiss = tiss
        self.pbTp = pbTp
        self.integTime = integTime
        self.gendr = gender
        self.ag = age
        self.thrpy = therapy
        self.tisWall = tissue_wall
        self.chng_pt = change_point
        self.pause = False
        self.ignoreSpectrum = 1
        self.pathtoexp = pathtoexp
        self.nts = nts

    def __del__(self):
        self.wait()

    def run(self):
        while True:
            if self.pause:
                print("Paused")
                while self.pause: time.sleep(1)
######################################################################################################################
            oldPath = self.pathtoexp + '/' + 'oldV'
            newPath = self.pathtoexp + '/' + 'newV'
            # print(oldPath, newPath)
            if not os.path.isdir(oldPath):
                os.makedirs(oldPath)
            if not os.path.isdir(newPath):
                os.makedirs(newPath)
            # self.speCounter += 1
            # dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
            # folder_exp_name = dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_0' + str(
            #     self.speCounter)
            # pathtoexp = outdir + '\\' + folder_exp_name
            # print(self.chng_pt)
            # if not os.path.isdir(pathtoexp):
            #     os.makedirs(pathtoexp)
            # img_name = pathtoexp + '\\' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.pbTp + '_' + \
            #            self.specRng + '_' + 'IntegrationTime' + '_' + str(
            #     self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)
            raw_spec = np.zeros((self.wavels.size, self.totSpecs))  # raw spectra
            drs_spec = np.zeros((self.wavels.size, self.totSpecs))  # drs/normalized spectra
            meandrs = np.zeros((self.wavels.size, 1))  # mean drs
            for i in range(0, self.totSpecs):
                spectralData = self.spectrometer.intensities(correct_dark_counts=True, correct_nonlinearity=True)
                specData = np.interp(self.wavels, self.specWavels, spectralData)
                raw_spec[:, i] = specData
                # print(specData)
                # print(len(specData))
                drs = (specData.reshape(self.wavels.size, 1) - self.darkNs) / (self.whtStd - self.darkNs)
                # print(len(drs))
                drs_spec[:, i] = drs.reshape(self.wavels.size)
#######################################################################################################
                drs_spec[:, i] = golayFilter(drs_spec[:, i], 15, 3)  # window length = 11 or 15
            meandrs = np.mean(drs_spec, axis=1)
            if self.specRng == 'VIS':
                meanIntensity = np.mean(meandrs[96:])  # 420-720
            else:
                meanIntensity = np.mean(meandrs[62:1333])  # 450-1100, 62:1333, 55:1627
            # print(meanIntensity)
            if meanIntensity < 0.001:
                self.ignoreSpectrum = 1  #1
                ignore = 'ignore'
                # self.speCounter += 1
                # self.ignoreSpectrum = 0
                # ignore = ''
                # dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
                # # folder_exp_name = dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_0' + str(
                # #     self.speCounter)
                # # pathtoexp = outdir + '\\' + folder_exp_name
                # # if not os.path.isdir(pathtoexp):
                # #     os.makedirs(pathtoexp)
                # img_name = self.pathtoexp + '\\' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.tisWall + '_' + self.pbTp + '_' + \
                #            self.specRng + '_' + 'IntegrationTime' + '_' + str(
                #     self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)
                # sio.savemat(img_name + '_ ' + ignore + '.mat',
                #             {'wavelength': self.wavels, 'drsSpectra': drs_spec, 'meanDrs': meandrs,
                #              'gender': self.gendr, 'age': self.ag, 'operation': self.opertn, 'therapy': self.thrpy,
                #              'organ': self.orgn, 'tissue': self.tiss, 'tissue_wall': self.tisWall,
                #              'spectral_range': self.specRng, 'probe_position': self.chng_pt})
                # self.drsSignal.emit(meandrs, self.speCounter)
            else:
                # print(self.speCounter)
                self.speCounter += 1
                self.ignoreSpectrum = 0
                ignore = ''
                dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
                # folder_exp_name = dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_0' + str(
                #     self.speCounter)
                # pathtoexp = outdir + '\\' + folder_exp_name
                # if not os.path.isdir(pathtoexp):
                #     os.makedirs(pathtoexp)
#########################################################################################################################
                # img_name = self.pathtoexp + '\\' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.tisWall + '_' + self.pbTp + '_' + \
                #            self.specRng + '_' + 'IntegrationTime' + '_' + str(
                #     self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)
                old_img_name = oldPath + '/' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.tisWall + '_' + self.pbTp + '_' + \
                           self.specRng + '_' + 'IntegrationTime' + '_' + str(
                    self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)
                new_img_name = newPath + '/' + dateexp + '_' + self.opertn + '_' + self.orgn + '_' + self.tiss + '_' + self.tisWall + '_' + self.pbTp + '_' + \
                               self.specRng + '_' + 'IntegrationTime' + '_' + str(
                    self.integTime * 0.8) + '_' + 'Position' + '_0' + str(self.speCounter)

                final_wavels = self.wavels.reshape(-1, 1)  # row vector
                final_drs_spec = drs_spec.reshape(-1, 1)  # row vector
                final_meandrs = meandrs.reshape(-1, 1)  # row vector
                # final_data = np.c_[final_wavels, final_drs_spec, final_meandrs, self.gendr, self.ag, self.opertn, self.thrpy, self.orgn, self.tiss, self.tisWall,
                #                    self.specRng, self.chng_pt, self.nts]
                # sio.savemat(img_name + '_' + ignore + '_newVersion' + '.mat', {final_data})
                final_data = np.array([final_wavels, final_drs_spec, final_meandrs])
                metaData = np.array([self.gendr, self.ag, self.opertn, self.thrpy, self.orgn, self.tiss, self.tisWall,
                                    self.specRng, self.chng_pt, self.nts])
                final_data2 = np.hstack((final_data, metaData))
###########################################################################################
                sio.savemat(new_img_name + '_' + ignore + '_newVersion' + '.mat', {'spectra': final_data, 'metadata': metaData, 'all': final_data2, 'total_spectra': drs_spec})
                new_version_name = new_img_name + '_' + ignore + '_newVersion' + '.json'
                newV_data = {}
                newV_data['wavelengths'] = final_wavels
                newV_data['raw_spectra'] = final_drs_spec
                newV_data['mean_spectra'] = final_meandrs
                newV_data['metadata'] = metaData
                with open(new_version_name, mode='w') as jfile:
                    jobj = json.dumps(newV_data, indent=2)
                    jfile.write(jobj)
                jfile.close()
########################################################################################
                sio.savemat(old_img_name + '_ ' + ignore + '.mat',
                            {'wavelength': self.wavels, 'drsSpectra': drs_spec, 'meanDrs': meandrs,
                             'gender': self.gendr, 'age': self.ag, 'operation': self.opertn, 'therapy': self.thrpy,
                             'organ': self.orgn, 'tissue': self.tiss, 'tissue_wall': self.tisWall,
                             'spectral_range': self.specRng, 'probe_position': self.chng_pt})
                # self.drsSignal.emit(meandrs, self.speCounter)
                self.drsSignal.emit(meandrs, self.speCounter, old_img_name)
            # print("Ignore is: {}".format(ignore))
            # sio.savemat(img_name + '_ ' + ignore + '.mat',
            #             {'wavelength': self.wavels, 'drsSpectra': drs_spec, 'meansDrs': meandrs})
            # self.drsSignal.emit(meandrs, self.speCounter)


class StartWindow(QMainWindow):
    def __init__(self):
        super().__init__()

        self.setCamPrms = None
        self.setExpPrms = None

        # Choose where to store the images, videos, tracking information and spectra
        self.dir_path = QFileDialog.getExistingDirectory(self, "Choose Directory", "C:\\")
        # print(self.dir_path)
        self.dir_path = self.dir_path + '/' + 'Data_' + time.strftime('%d%m%Y')
        if not os.path.isdir(self.dir_path):
            os.makedirs(self.dir_path)

        global isTracking
        isTracking = 0
        # self.camera = camera
        # self.timer = QTimer()
        # self.timer.timeout.connect(self.nextFrameSlot)
        self.cameraNum = 1
        self.brightness = 80
        self.focus = 15
        self.fps = 30
        # self.spec = None
        pixmap = QPixmap('black.jpg')
        self.acquisition = 0
        self.countSpec = 0
        self.totalSpectra = 20
        self.pts = deque()
        # self.probePos = None
        self.isWorking = False
        codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
        self.out_video_raw = cv2.VideoWriter(
            self.dir_path + '/' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_output_raw' + '.avi', codec,
            30, (640, 480), True)
        self.out_video_proc = cv2.VideoWriter(
            self.dir_path + '/' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '_output_proc' + '.avi', codec,
            30, (640, 480), True)
        self.change_point = None
        self.specThread = None
        self.frameth = None
        self.calibrationDone = False
        self.expCounter = 0
#############################################################################
        self.tissueChanged = False
        self.classification = False
        self.classLabel = None
        self.inSpectrum = None
        self.ready4classif = False
        self.ptsClassif = deque()
        self.pcl = deque()
        self.classifPtsALL = deque()
        self.pclALL = deque()
        self.ptsALL = deque()

        self.video_frame = QLabel()
        self.video_frame.setFixedSize(640, 480)
        self.video_frame.setPixmap(pixmap)

        self.graphWidget = pg.PlotWidget()
        self.graphWidget.setFixedSize(640, 480)
        self.graphWidget.hideButtons()
        self.graphWidget.setTitle("Acquired Spectrum", color='#ffffff', size="16px")  # red colour: #FF0000, white colour: #ffffff
        styles = {'color': '#ffffff', 'font-size': '16px'}
        self.graphWidget.setLabel('left', 'Reflectance', units='a.u.', **styles)
        self.graphWidget.setLabel('bottom', 'Wavelength', units='nm', **styles)
        # self.graphWidget.setXRange(420, 1000, padding=0)
        # self.graphWidget.setYRange(0, 1.2, padding=0)
        self.graphWidget.getAxis('left').enableAutoSIPrefix(False)
        self.graphWidget.getAxis('bottom').enableAutoSIPrefix(False)
        self.graphWidget.showGrid(x=True, y=True)
        pen = pg.mkPen(color=(255, 0, 0), width=3)
        self.line_ref = self.graphWidget.plot(pen=pen, symbol='o', symbolSize=5, symbolBrush=('b'))
        self.exporter = pg.exporters.ImageExporter(self.graphWidget.plotItem)
        self.exporter.parameters()['width'] = 640

        self.centralWidget = QWidget(self)
        self.centralWidget.resize(1366, 768)
        layout = QGridLayout()
        self.centralWidget.setLayout(layout)
        layout.addWidget(self.video_frame, 0, 0)
        layout.addWidget(self.graphWidget, 0, 1)
        # grid.addWidget(self.video_frame, 2, 2)
        # self.layout = QHBoxLayout(self.centralWidget)
        # self.layout.addWidget(self.video_frame)
        # # self.layout.addWidget(self.video_frame2)
        # self.layout.addWidget(self.view)
        self.setCentralWidget(self.centralWidget)
        # self.setLayout(self.layout)

        self.exitAct = QAction(QIcon('exit.png'), 'Exit', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.exitAct.setStatusTip('Exit Application')
        self.exitAct.triggered.connect(self.terminateApp)

        self.setCalibration = QAction(QIcon('calibration.png'), 'Calibration', self)
        # setCalibration.setShortcut('Ctrl+Q')
        self.setCalibration.setStatusTip('DRS Probe Calibration')
        self.setCalibration.triggered.connect(self.doCalibration)

        self.startCamera = QAction(QIcon('startVideo.png'), 'Start Camera', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.startCamera.setStatusTip('Start Camera')
        self.startCamera.triggered.connect(self.camera_init)
        self.startCamera.setDisabled(True)

        self.stopCamera = QAction(QIcon('stopVideo.png'), 'Stop Camera', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stopCamera.setStatusTip('Close Camera')
        self.stopCamera.triggered.connect(self.camera_close)
        self.stopCamera.setDisabled(True)

        self.capture = QAction(QIcon('capture.png'), 'Capture Image', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.capture.setStatusTip('Capture Image')
        self.capture.triggered.connect(self.capture_image)
        self.capture.setDisabled(True)

        self.setCamParams = QAction(QIcon('camParams.png'), 'Camera Parameters', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.setCamParams.setStatusTip('Set Camera Parameters')
        self.setCamParams.triggered.connect(self.setCamera)
        self.setCamParams.setDisabled(True)

        self.setExpParams = QAction(QIcon('parameters.png'), 'Experiment Parameters', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.setExpParams.setStatusTip('Set Experiment Parameters')
        self.setExpParams.triggered.connect(self.setExperiment)
        # self.setExpParams.setDisabled(True)

        self.iniTracking = QAction(QIcon('startTracking.png'), 'Track', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.iniTracking.setStatusTip('Start Tracking DRS Probe')
        self.iniTracking.triggered.connect(self.startTracking)
        self.iniTracking.setDisabled(True)

        self.stpTracking = QAction(QIcon('stopTracking.png'), 'No Track', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stpTracking.setStatusTip('Stop Tracking DRS Probe')
        self.stpTracking.triggered.connect(self.stopTracking)
        self.stpTracking.setDisabled(True)

        self.initAcquisition = QAction(QIcon('startSpectrum.png'), 'Acquire Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.initAcquisition.setStatusTip('Start Spectrum Acquisition')
        self.initAcquisition.triggered.connect(self.startAcquisition)
        self.initAcquisition.setDisabled(True)

        self.stpAcquisition = QAction(QIcon('pause.png'), 'Pause Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.stpAcquisition.setStatusTip('Pause Spectrum Acquisition')
        self.stpAcquisition.triggered.connect(self.stopAcquisition)
        self.stpAcquisition.setDisabled(True)

        self.endAcquisition = QAction(QIcon('stopSpectrum.png'), 'End Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.endAcquisition.setStatusTip('End Spectrum Acquisition')
        self.endAcquisition.triggered.connect(self.endOfAcquisition)
        self.endAcquisition.setDisabled(True)

        self.contAcquisition = QAction(QIcon('play.png'), 'Resume Spectrum', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.contAcquisition.setStatusTip('Resume Spectrum Acquisition')
        self.contAcquisition.triggered.connect(self.continueAcquisition)
        self.contAcquisition.setDisabled(True)

        self.lightOn = QAction(QIcon('lightON.png'), 'Light On', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.lightOn.setStatusTip('Turn Light On')
        self.lightOn.triggered.connect(self.turnLightOn)
        self.lightOn.setDisabled(True)

        self.lightOff = QAction(QIcon('lightOFF.png'), 'Light Off', self)
        # exitAct.setShortcut('Ctrl+Q')
        self.lightOff.setStatusTip('Turn Light Off')
        self.lightOff.triggered.connect(self.turnLightOff)
        self.lightOff.setDisabled(True)

        # menubar = self.menuBar()
        # cameraMenu = menubar.addMenu('&Camera')
        # # cameraMenu.addSeparator()
        # cameraMenu.addAction(closeCam)

        toolbar = self.addToolBar('Toolbar')
        toolbar.addAction(self.exitAct)
        toolbar.addSeparator()
        toolbar.addAction(self.setCalibration)
        toolbar.addAction(self.setExpParams)
        toolbar.addSeparator()
        toolbar.addAction(self.lightOn)
        toolbar.addAction(self.lightOff)
        toolbar.addSeparator()
        toolbar.addAction(self.startCamera)
        # toolbar.addSeparator()
        toolbar.addAction(self.stopCamera)
        # toolbar.addSeparator()
        toolbar.addAction(self.capture)
        # toolbar.addSeparator()
        toolbar.addAction(self.setCamParams)
        toolbar.addSeparator()
        toolbar.addAction(self.iniTracking)
        toolbar.addAction(self.stpTracking)
        toolbar.addSeparator()
        toolbar.addAction(self.initAcquisition)
        toolbar.addAction(self.contAcquisition)
        toolbar.addAction(self.stpAcquisition)
        toolbar.addAction(self.endAcquisition)
        toolbar.setIconSize(QSize(50, 50))
        toolbar.setStyleSheet("QToolBar{spacing:10px;}")

        self.statusBar().showMessage('Ready')
        self.setGeometry(0, 0, 1366, 768)
        self.setWindowTitle('DRS Imaging System')
        self.setWindowIcon(QIcon('icon02.jpg'))
        self.showMaximized()

    def camera_init(self):
        self.brightness = 160
        self.frameth = cameraThread(self.cameraNum, self.brightness, self.focus, self.fps, self.dir_path)
        # self.capture.triggered(self.frameth.capture)
        self.frameth.frameSignal.connect(self.nextFrameSlot)
        # self.frameth.imgSignal.connect(self.saveAnnotImg)
        # self.frameth.capSignal.connect(self.makeVideo)
        # self.frameth.capSignal.connect(self.camera_close)
        self.frameth.start()
        # self.cap = self.camera.start_camera()
        # if not self.cap.isOpened():
        #     msgBox = QMessageBox()
        #     msgBox.setIcon(QMessageBox.Critical)
        #     msgBox.setWindowTitle("Failure")
        #     msgBox.setText("Failed to open camera.")
        #     msgBox.exec_()
        #     return
        # self.camera.set_camera_parameters(self.brightness, self.focus, self.fps)
        # self.out_video = self.camera.write_video(rootdir, 'output_raw')
        # self.out_video_proc = self.camera.write_video(rootdir, 'output_processed')
        if not self.calibrationDone:
            devices = list_devices()
            # print(devices)
            self.spec = Spectrometer(devices[0])
            # self.spec = Spectrometer.from_first_available()
            self.spec.integration_time_micros(self.integration_time * 0.8)
            self.wavelengths = self.spec.wavelengths()
        # devices = list_devices()
        # print(devices)
        # self.spec = Spectrometer(devices[0])
        # self.spec = Spectrometer.from_first_available()
        # self.spec.integration_time_micros(self.integration_time * 0.8)
        # self.wavelengths = self.spec.wavelengths()
        # self.timer.start(1000 / self.fps)
        self.startCamera.setDisabled(True)
        self.stopCamera.setDisabled(False)
        self.capture.setDisabled(False)
        self.setCamParams.setDisabled(False)
        self.iniTracking.setDisabled(False)
        self.initAcquisition.setDisabled(False)

    # def makeVideo(self, cap):
    #     codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
    #     self.out_video_raw = cv2.VideoWriter(
    #         outdir + '\\' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + 'output_raw' + '.avi', codec,
    #         cap.get(cv2.CAP_PROP_FPS), (int(cap.get(3)), int(cap.get(4))), True)
    #     self.out_video_proc = cv2.VideoWriter(
    #         outdir + '\\' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + 'output_proc' + '.avi', codec,
    #         cap.get(cv2.CAP_PROP_FPS), (int(cap.get(3)), int(cap.get(4))), True)

    def loadCalibration(self):
        if self.probe == "Standard":
            if self.spectral_range == 'VIS':
                self.myRGB_WLx = np.arange(start=420, stop=720.5, step=0.5)
                darknoise = sio.loadmat('calib_standard_probe\darknoiseVIS.mat')
                self.darknoise = darknoise['darknoise']
                whitestd = sio.loadmat('calib_standard_probe\whitestdVIS.mat')
                self.whitestd = whitestd['whitestd']
                self.graphWidget.setXRange(400, 750, padding=0)
                self.graphWidget.setYRange(0, 1.2, padding=0)
            elif self.spectral_range == 'VNIR':
                self.myRGB_WLx = np.arange(start=450, stop=1100, step=0.33829)
                darknoise = sio.loadmat('calib_standard_probe\darknoiseVNIR.mat')
                self.darknoise = darknoise['darknoise']
                whitestd = sio.loadmat('calib_standard_probe\whitestdVNIR.mat')
                self.whitestd = whitestd['whitestd']
                self.graphWidget.setXRange(400, 1200, padding=0)
                self.graphWidget.setYRange(0, 1.2, padding=0)
        elif self.probe == "Sterilised":
            if self.spectral_range == 'VIS':
                self.myRGB_WLx = np.arange(start=420, stop=720.5, step=0.5)
                darknoise = sio.loadmat('calib_sterile_probe\darknoiseVIS.mat')
                self.darknoise = darknoise['darknoise']
                whitestd = sio.loadmat('calib_sterile_probe\whitestdVIS.mat')
                self.whitestd = whitestd['whitestd']
                self.graphWidget.setXRange(400, 750, padding=0)
                self.graphWidget.setYRange(0, 1.2, padding=0)
            elif self.spectral_range == 'VNIR':
                self.myRGB_WLx = np.arange(start=450, stop=1100, step=0.33829)
                darknoise = sio.loadmat('calib_sterile_probe\darknoiseVNIR.mat')
                self.darknoise = darknoise['darknoise']
                whitestd = sio.loadmat('calib_sterile_probe\whitestdVNIR.mat')
                self.whitestd = whitestd['whitestd']
                self.graphWidget.setXRange(400, 1200, padding=0)
                self.graphWidget.setYRange(0, 1.2, padding=0)
        # if self.spectral_range == 'VIS':
        #     self.myRGB_WLx = np.arange(start=420, stop=720.5, step=0.5)
        #     darknoise = sio.loadmat('darknoiseVIS.mat')
        #     self.darknoise = darknoise['darknoise']
        #     whitestd = sio.loadmat('whitestdVIS.mat')
        #     self.whitestd = whitestd['whitestd']
        #     self.graphWidget.setXRange(400, 750, padding=0)
        #     self.graphWidget.setYRange(0, 1.2, padding=0)
        # elif self.spectral_range == 'VNIR':
        #     self.myRGB_WLx = np.arange(start=450, stop=1100, step=0.33829)
        #     darknoise = sio.loadmat('darknoiseVNIR.mat')
        #     self.darknoise = darknoise['darknoise']
        #     whitestd = sio.loadmat('whitestdVNIR.mat')
        #     self.whitestd = whitestd['whitestd']
        #     self.graphWidget.setXRange(400, 1200, padding=0)
        #     self.graphWidget.setYRange(0, 1.2, padding=0)
##########################################################################################
        # if self.expParams and self.specThread is not None:
        #     self.specThread.darkNs = self.darknoise
        #     self.specThread.whtStd = self.whitestd
        # Load stomach classifier
        if self.organ == 'Stomach' and self.classification == True:
            self.classifier = joblib.load('stomachClassifier.pkl')
            self.stdScaler = joblib.load('std_scaler_stomach.pkl')
        elif self.organ == 'Oesophagus' and self.classification == True:
            self.classifier = joblib.load('oesophagusClassifier.pkl')
            self.stdScaler = joblib.load('std_scaler_oesophagus.pkl')
        msg1 = QMessageBox()
        msg1.setIcon(QMessageBox.Information)
        msg1.setWindowTitle("Success")
        msg1.setText("Calibration data for {} spectral range successfully loaded. Classification of {} is {}.".format(self.spectral_range, self.organ, self.classify))
        msg1.exec_()
        if self.expCounter == 0:
            msg2 = QMessageBox()
            msg2.setIcon(QMessageBox.Warning)
            msg2.setWindowTitle("Warning")
            msg2.setText("Don't forget to turn ON the light source!")
            msg2.exec_()
        return

    def nextFrameSlot(self, frame, change_point):
        global isTracking
        # ret, self.frame = self.camera.get_frame()
        # self.frame = cv2.flip(self.frame, 1)
        self.capFrame = frame
        self.out_video_raw.write(self.capFrame)
        frm = frame

        if self.tissueChanged:
            if self.classification:
                self.ptsClassif.clear()
                self.pcl.clear()
            else:
                self.pts.clear()
            self.tissueChanged = False

        # if ret:
        if isTracking == 1:
            self.change_point = change_point
            cv2.circle(frm, self.change_point, 3, (255, 0, 0), -1)
            # print(self.acquisition)
            if self.acquisition == 1:
                # self.change_point = change_point
                self.specThread.start()
                self.specThread.chng_pt = self.change_point
                # print(self.change_point)
                # print("Spectrum thread is {}".format(self.specThread.isRunning()))
                # print("Working is {}".format(self.isWorking))
                # print(self.specThread.ignoreSpectrum)
                if self.isWorking is True and self.specThread.ignoreSpectrum == 0 and \
                        self.frameth.ROISelector.poly.contains(
                            geometry.Point(self.change_point[0], self.change_point[1])):
                # if self.isWorking is True and \
                #         self.frameth.ROISelector.poly.contains(
                #             geometry.Point(self.change_point[0], self.change_point[1])):
                # if self.isWorking is True:
                    # cv2.putText(frm, self.countspec, (10, 10), cv2.FONT_HERSHEY_DUPLEX, 1, (255, 0, 255), 1)
                    # print(True)
                    # print("Inner Working {}".format(self.isWorking))
                    # if self.ignoreSpectrum == 0:
                    # self.probePos = self.change_point
###################################################################################################################################
                    prPs = (self.change_point[0], self.change_point[1])  # - 3)

                    if self.classification and self.ready4classif:
                        pred = self.classifier.predict(self.inSpectrum)  # get class prediction for the incoming spectrum - 0: NS, 1: NO, 2: TS, 3: TO
                        prob = self.classifier.predict_proba(self.inSpectrum)
                        # Get max prob value and its index. Based on that probability get the corresponding colour from the colour pallette
                        maxVal = np.max(prob)
                        maxIndx = round(np.argmax(prob), 2)  # 2 decimal points - gives either 0 or 1 (binary classification)
                        # print(pred, prob)
                        if maxIndx == 0:  # if normal - max colour green
                            cl = cm.winter(maxVal)
                        elif maxIndx == 1:  # if tumour - max colour pink
                            cl = cm.cool(maxVal)
                        pcolour = (int(cl[0] * 255), int(cl[1] * 255), int(cl[2] * 255))
                        # print(pred, prob, pcolour)
                        # pcl.appendleft(pcolour)
                        if self.organ == 'Stomach':
                            if pred == 0:
                                self.classLabel = 'Normal Stomach'
                            elif pred == 2:
                                self.classLabel = 'Tumour Stomach'
                        elif self.organ == 'Oesophagus':
                            if pred == 1:
                                self.classLabel = 'Normal Oesophagus'
                            elif pred == 3:
                                self.classLabel = 'Tumour Oesophagus'
                        cv2.circle(frm, prPs, 3, pcolour, -1)
                        cv2.putText(frm, self.classLabel, (25, 150), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (255, 255, 0), 5, cv2.LINE_AA)
                        # print(self.classLabel)
                        self.f5.write(str(self.countSpec) + '.' + '' + self.classLabel + '-' + str(maxVal) + '\n')
                        self.ptsClassif.appendleft(prPs)
                        self.pcl.appendleft(pcolour)
                        self.pclALL.appendleft(pcolour)
                        self.classifPtsALL.appendleft(prPs)
                        sio.savemat(self.labelNameFile + '_Label' + '.mat',
                                    {'label': pred, 'labelName': self.classLabel, 'probability': prob})
                        self.saveAnnotImg(pcolour)
                    else:
                        self.pts.appendleft(prPs)
                        self.ptsALL.appendleft(prPs)
                        self.saveAnnotImg((0, 255, 0))

                    # self.pts.appendleft(prPs)
                    # self.saveProbePosition()
                    cv2.putText(frm, str(self.countSpec), (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1,
                            (255, 0, 0), 5, cv2.LINE_AA)
                    # self.saveAnnotImg()
                    # f = open(outdir + '\\' + time.strftime("%d%m%Y") +'_' + self.organ + '_' + self.tissue + '_ProbePosition.txt', 'a')
                    # self.f.write(str(countspec) + '.' + '' + str(change_point) + '\n')
                    # f.close()
                    # cv2.ellipse(self.capturedImage, (probePos[0]+3, probePos[1]-2), (1, 1), 0, 0, 360, (0, 0, 255), -1)
                    # cv2.putText(self.capturedImage, str(self.countspec), prPs, cv2.FONT_HERSHEY_DUPLEX, 0.2, (255, 255, 255), 1)
                    # img_name = self.image_path + '\\' + self.date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_Annotated.png'
                    # cv2.imwrite(img_name, self.capturedImage)
                    # self.graphWidget.setTitle("Acquired Spectrum {}".format(self.countspec), color='#FF0000', size="16px")
                    # self.line_ref.setData(self.myRGB_WLx, self.meandrs)
                    # self.exporter.export(self.img_name + '.png')
                    # self.countspec += 1
                    self.isWorking = False
                    self.ready4classif = False
        # print(self.frameth.roi)
        for i in range(0, len(self.pts)):
            if self.pts[i] is None:
                continue
################################################################################
            if self.classification == False:
                cv2.circle(frm, self.pts[i], 3, (0, 255, 0), -1)
            # cv2.circle(frm, self.pts[i], 3, (0, 255, 0), -1)
        for i in range(0, len(self.ptsClassif)):
            if self.ptsClassif[i] is None:
                continue
            else:
                cv2.circle(frm, self.ptsClassif[i], 3, self.pcl[i], -1)
        self.out_video_proc.write(frm)
##########################################################################
        # self.frm = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        self.frm = cv2.cvtColor(frm, cv2.COLOR_BGR2RGB)
        image = QImage(self.frm, self.frm.shape[1], self.frm.shape[0], QImage.Format_RGB888)
        pixmap = QPixmap.fromImage(image)
        self.video_frame.setPixmap(pixmap)

    # def plotSpectrum(self, spectrum, countspec):
    def plotSpectrum(self, spectrum, countspec, matName):
        self.labelNameFile = matName
        self.countSpec = countspec
        self.isWorking = True
        ##############################################################################################
        if self.classification == True:
            if self.spectral_range == 'VIS':
                self.inSpectrum = spectrum[96:]  # Get wavelengths information from 468 - 720nm for classification
                # print(self.inSpectrum)
            else:
                self.inSpectrum = spectrum
                # print(len(self.inSpectrum))
                self.inSpectrum = self.binSpectrum(self.inSpectrum)
                # self.inSpectrum = spectrum[55:1627]
                # print(self.inSpectrum)
            self.inSpectrum = self.inSpectrum.reshape(1, -1)  # not sure if this needed
            self.inSpectrum = self.stdScaler.transform(self.inSpectrum)
            # print(self.inSpectrum)
            # self.inSpectrum = StandardScaler().fit_transform(self.inSpectrum)
            # print(self.inSpectrum)
            # print(self.inSpectrum.shape)
            self.ready4classif = True
        self.graphWidget.setTitle("Acquired Spectrum {}".format(self.countSpec), color='#ffffff', size="16px")
        self.line_ref.setData(self.myRGB_WLx, spectrum)
        self.saveProbePosition()

    def binSpectrum(self, spectrum):
        bin = 0.5
        range1 = np.arange(start=450, stop=720.5, step=0.5)
        range2 = np.arange(start=450, stop=721, step=0.33829)
        range3 = np.arange(start=451, stop=721.5, step=0.5)
        ypartmean = np.zeros(len(range1))
        drs2c = np.zeros((len(range1), ))
        drs2d = np.zeros((505, ))
        # print(drs2c.shape, ypartmean.shape, drs2d.shape)
        x = range2
        y = np.array(spectrum[:802])  # up to 721nm
        # print(y.shape)
        partitions = [np.transpose(range1), np.transpose(range3)]
        for k1 in range(len(range1)):
            # print((x >= partitions[0][k1]) & (x < partitions[1][k1]))
            indexTrue = (x >= partitions[0][k1]) & (x < partitions[1][k1])
            yp = y[indexTrue]
            # print(yp)
            ypartmean[k1] = np.mean(yp)
            # print(k1)
            # ypartmean[k1] = np.mean(y((x >= partitions[0][k1]) & (x < partitions[1][k1])))
        # print(ypartmean)
        drs2c = ypartmean
        # print(drs2c)
        # print(drs2c.shape, drs2c)
        drs2d = drs2c[36:]  # from 468nm
        # print(drs2d.shape)
        return drs2d

    def saveProbePosition(self):
        # global capturedImage
        # f = open(outdir + '\\' + time.strftime("%d%m%Y") + '_' + self.organ + '_' + self.tissue + '_ProbePosition.txt', 'a')
        self.f.write(str(self.countSpec) + '.' + '' + str(self.change_point) + '\n')
        self.f2.write(str(self.change_point[0]) + ',' + str(self.change_point[1]) + '\n')
        self.f3.write(str(self.change_point[0]) + '\n')
        self.f4.write(str(self.change_point[1]) + '\n')
######################################################################################################################
        # if self.classification and self.ready4classif:
        #     self.f5.write(str(self.countSpec) + '.' + '' + self.classLabel + '\n')
        # f.close()
        # cv2.ellipse(capturedImage, (self.change_point[0] + 3, self.change_point[1] - 2), (1, 1), 0, 0, 360, (0, 0, 255), -1)
        # # cv2.putText(self.capturedImage, str(self.countspec), prPs, cv2.FONT_HERSHEY_DUPLEX, 0.2, (255, 255, 255), 1)
        # img_name = outdir + '\\' + self.date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_Annotated.png'
        # cv2.imwrite(img_name, capturedImage)

    def saveAnnotImg(self, colour):
        global capturedImage
####################################################################################
        date_time = datetime.now().strftime('%Y-%m-%d')
        # cv2.ellipse(capturedImage, (self.change_point[0] + 3, self.change_point[1] - 2), (1, 1), 0, 0, 360, (255, 0, 0),
        #             -1)
        cv2.circle(capturedImage, (self.change_point[0] + 3, self.change_point[1] - 2), 3, colour, -1)
        # cv2.putText(self.capturedImage, str(self.countspec), prPs, cv2.FONT_HERSHEY_DUPLEX, 0.2, (255, 255, 255), 1)
#############################################################################################################
        img_name = self.dir_path + '/' + date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_' + self.tissue_wall + '_' + self.spectral_range + '_' + self.classify + '_Annotated.png'
        # img_name = outdir + '\\' + date_time + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_Annotated.png'
        cv2.imwrite(img_name, capturedImage)

    def startTracking(self):
        global isTracking
        isTracking = 1
        self.iniTracking.setDisabled(True)
        self.stpTracking.setDisabled(False)

    def stopTracking(self):
        global isTracking
        isTracking = 0
        self.iniTracking.setDisabled(False)
        self.stpTracking.setDisabled(True)

    def startAcquisition(self):
        self.acquisition = 1
        self.isWorking = True
        # self.specThread.pause = False
        self.f = open(
            self.dir_path + '/' + time.strftime("%d%m%Y") + '_' + self.operation + '_' + '_ProbePosition.txt',
            'a')
        self.f2 = open(
            self.dir_path + '/' + time.strftime("%d%m%Y") + '_' + self.operation + '_' + '_Probe_Position_new.txt',
            'a')
        self.f3 = open(
            self.dir_path + '/' + time.strftime("%d%m%Y") + '_' + self.operation + '_' + '_Probe_X_Position.txt',
            'a')
        self.f4 = open(
            self.dir_path + '/' + time.strftime("%d%m%Y") + '_' + self.operation + '_' + '_Probe_Y_Position.txt',
            'a')
######################################################################################################################
        self.f5 = open(
            self.dir_path + '/' + time.strftime("%d%m%Y") + '_' + self.operation + '_' + '_Classification_Label.txt',
            'a')
        self.specThread = spectrumThread(self.spec, self.myRGB_WLx, self.countSpec, self.totalSpectra, self.wavelengths,
                                         self.darknoise,
                                         self.whitestd, self.spectral_range, self.operation, self.organ, self.tissue,
                                         self.probe_type, self.integration_time, self.gender, self.age, self.therapy,
                                         self.tissue_wall, self.change_point, self.pathtoexp, self.notes)
        self.specThread.drsSignal.connect(self.plotSpectrum)
        # self.frameth.imgSignal.connect(self.saveAnnotImg)
        # self.specThread.drsSignal.connect(self.saveProbePosition)
        self.initAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(False)
        self.endAcquisition.setDisabled(False)
        self.lightOff.setDisabled(True)

    def stopAcquisition(self):
        self.specThread.pause = True
        # self.graphWidget.clear()
        # self.specThread.wait()
        # self.specThread.terminate()
        self.acquisition = 0
        # self.f.close()
        # self.graphWidget.setTitle("Acquired Spectrum", color='#FF0000', size="16px")
        # self.initAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(True)
        self.contAcquisition.setDisabled(False)

    def continueAcquisition(self):
        if self.expParams:
            self.specThread.specRng = self.spectral_range
            self.specThread.opertn = self.operation
            self.specThread.orgn = self.organ
            self.specThread.tiss = self.tissue
            self.specThread.pbTp = self.probe_type
            self.specThread.integTime = self.integration_time
            self.specThread.tisWall = self.tissue_wall
            self.specThread.thrpy = self.therapy
            self.specThread.ag = self.age
            self.specThread.gendr = self.gender
            self.specThread.pathtoexp = self.pathtoexp
            self.specThread.nts = self.notes
############################################################################
            self.specThread.darkNs = self.darknoise
            self.specThread.whtStd = self.whitestd
            self.specThread.wavels = self.myRGB_WLx
        self.specThread.pause = False
        self.acquisition = 1
        # self.initAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(False)
        self.contAcquisition.setDisabled(True)

    def endOfAcquisition(self):
        self.specThread.terminate()
        self.acquisition = 0
        self.f.close()
        self.f2.close()
        self.f3.close()
        self.f4.close()
######################################################################################################################
        self.f5.close()
        self.graphWidget.setTitle("Acquired Spectrum", color='#FF0000', size="16px")
        self.graphWidget.clear()
        self.initAcquisition.setDisabled(True)
        self.endAcquisition.setDisabled(True)
        self.lightOff.setDisabled(False)

    def turnLightOn(self):
        ser = serial.Serial(
            port='COM3',
            baudrate=9600,
            timeout=1,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False,
            rtscts=False,
            dsrdtr=False,
            writeTimeout=2
        )
        if ser.isOpen():
            ser.write('SO\r'.encode())  # SO: turn on, CO: turn off
            ser.flushInput()
            bytesToRead = ser.inWaiting()
            data = ser.read(bytesToRead)
            self.lightOn.setDisabled(True)
            self.lightOff.setDisabled(False)
        else:
            print("cannot open serial port ")

    def turnLightOff(self):
        ser = serial.Serial(
            port='COM3',
            baudrate=9600,
            timeout=1,
            parity=serial.PARITY_NONE,
            stopbits=serial.STOPBITS_ONE,
            bytesize=serial.EIGHTBITS,
            xonxoff=False,
            rtscts=False,
            dsrdtr=False,
            writeTimeout=2
        )
        if ser.isOpen():
            ser.write('CO\r'.encode())  # SO: turn on, CO: turn off
            ser.flushInput()
            bytesToRead = ser.inWaiting()
            data = ser.read(bytesToRead)
            self.lightOff.setDisabled(True)
            self.lightOn.setDisabled(False)
        else:
            print("cannot open serial port ")

    def camera_close(self):
        # global cap
        self.frameth.stop()
        self.frameth.terminate()
        # cap.release()
        # self.frameth.isFinished()
        # self.frameth.terminate()
        # self.frameth.isFinish()
        # self.camera.stop_camera()
        # self.timer.stop()
        pixmap = QPixmap('black.jpg')
        self.video_frame.setPixmap(pixmap)
        self.startCamera.setDisabled(False)
        self.setCamParams.setDisabled(True)
        self.capture.setDisabled(True)
        self.stopCamera.setDisabled(True)
        self.initAcquisition.setDisabled(True)
        self.iniTracking.setDisabled(True)
        self.stpAcquisition.setDisabled(True)
        self.stpAcquisition.setDisabled(True)
        self.contAcquisition.setDisabled(True)
        self.endAcquisition.setDisabled(True)
        self.acquisition = 0
        self.tracking = 0
        msg1 = QMessageBox()
        msg1.setIcon(QMessageBox.Warning)
        msg1.setWindowTitle("Warning")
        msg1.setText("Do not forget to TURN OFF the light.")
        msg1.exec_()

    def capture_image(self):
        self.frameth.capture()
        self.frameth.selectROI()
        # print(self.frameth.ROISelector.poly.contains(geometry.Point(150, 240)))

    #     self.date_time = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
    #     self.image_path = outdir
    #     image_name = self.image_path + '\\' + self.date_time + '.png'
    #     success = cv2.imwrite(image_name, frame)
    #     self.capturedImage = frame
    #     qtimage = QImage(frame, frame.shape[1], frame.shape[0], QImage.Format_RGB888)
    #     self.pixmap_image = QPixmap.fromImage(qtimage)
    #     if success:
    #         msg = QMessageBox()
    #         msg.setIcon(QMessageBox.Information)
    #         msg.setWindowTitle("Success")
    #         msg.setText("Image captured and saved in \n {}.".format(self.image_path))
    #         msg.exec_()
    #         self.iniTracking.setDisabled(False)
    #         self.initAcquisition.setDisabled(False)
    #         return

    def setCamera(self):
        self.setCamPrms = Example()
        self.setCamPrms.show()
        self.setCamPrms.ok_button.clicked.connect(self.getCameraParams)

    def getCameraParams(self):
        # global cap
        self.brightness = self.setCamPrms.brightness_sld.value()
        self.focus = self.setCamPrms.focus_sld.value()
        self.frameth.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, True)
        self.frameth.cap.set(cv2.CAP_PROP_AUTOFOCUS, False)
        self.frameth.cap.set(cv2.CAP_PROP_BRIGHTNESS, self.brightness)
        self.frameth.cap.set(cv2.CAP_PROP_FOCUS, self.focus)
        self.frameth.cap.set(cv2.CAP_PROP_FPS, self.fps)
        # self.camera.set_camera_parameters(self.brightness, self.focus, self.fps)
        if self.brightness == 0 or self.focus == 0:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Camera brightness and/or focus value(s) cannot be 0.")
            msg.exec_()
            return
        else:
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Parameters successfully passed.")
            msg.exec_()
            self.setCamPrms.close_button.setDisabled(False)
            return

    def doCalibration(self):
        devices = list_devices()
        print(devices)
        self.spec = Spectrometer(devices[0])
        # print(self.spec.integration_time_micros_limits)
        self.wavelengths = self.spec.wavelengths()
        # print(self.wavelengths)
        # self.spec = Spectrometer.from_first_available()
        self.calibdialog = QDialog()
        self.calibdialog.ui = Ui_Dialog2()
        self.calibdialog.ui.setupUi(self.calibdialog)
        self.calibdialog.show()
        self.calibdialog.ui.setCalibration.clicked.connect(self.calibrate)

    def calibrate(self):
        self.integration_time = int(self.calibdialog.ui.integration_time_lineEdit.text())
        self.spectral_range = str(self.calibdialog.ui.spectral_range_comboBox.currentText())
        self.spec.integration_time_micros(self.integration_time * 0.8)
        # print(self.integration_time*0.8)
        if self.spectral_range == 'VIS':
            self.myRGB_WLx = np.arange(start=420, stop=720.5, step=0.5)
            self.graphWidget.setXRange(400, 750, padding=0)
            self.graphWidget.setYRange(0, 1.2, padding=0)
        elif self.spectral_range == 'VNIR':
            self.myRGB_WLx = np.arange(start=450, stop=1100, step=0.33829)
            self.graphWidget.setXRange(400, 1200, padding=0)
            self.graphWidget.setYRange(0, 1.2, padding=0)
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Information")
        msg.setText("Getting dark noise! Please turn off the lights and light source.")
        msg.exec_()
        raw_spec = np.zeros((self.myRGB_WLx.size, 400))  # raw spectra
        meandark = np.zeros((self.myRGB_WLx.size, 1))  # mean dark noise
        for i in range(0, 400):
            spectralData = self.spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)
            specData = np.interp(self.myRGB_WLx, self.wavelengths, spectralData)
            raw_spec[:, i] = specData
        meandark = np.mean(raw_spec, axis=1).reshape(-1, 1)
        sio.savemat(self.dir_path + '/' + 'darknoise' + self.spectral_range + '.mat',
                    {'wavelength': self.myRGB_WLx, 'darknoiseSpectra': raw_spec, 'darknoise': meandark})
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Information")
        msg.setText("Getting white standard! Please turn on the light source.")
        msg.exec_()
        meanwhite = np.zeros((self.myRGB_WLx.size, 1))  # mean white standard
        for i in range(0, 400):
            spectralData = self.spec.intensities(correct_dark_counts=True, correct_nonlinearity=True)
            specData = np.interp(self.myRGB_WLx, self.wavelengths, spectralData)
            raw_spec[:, i] = specData
        meanwhite = np.mean(raw_spec, axis=1).reshape(-1, 1)
        sio.savemat(self.dir_path + '/' + 'whitestd' + self.spectral_range + '.mat',
                    {'wavelength': self.myRGB_WLx, 'whitestdSpectra': raw_spec, 'whitestd': meanwhite})
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("Information")
        msg.setText("Calibration finished successfully!")
        msg.exec_()
        self.calibdialog.ui.calibration_buttonBox.button(QDialogButtonBox.Ok).setDisabled(False)
        self.calibrationDone = True

    def setExperiment(self):
        self.dialog = QDialog()
        self.dialog.ui = Form()
        self.dialog.ui.setupUi(self.dialog)
        self.dialog.show()
        self.dialog.ui.getParams_pushbutton.clicked.connect(self.getExperimentParams)

    def increaseExpCounter(self):
        self.expCounter += 1

    def getExperimentParams(self):
        self.expParams = False
        self.gender = self.dialog.ui.gender_comboBox.currentText()
        self.age = self.dialog.ui.age_lineEdit.text()
        self.therapy = self.dialog.ui.therapy_comboBox.currentText()
        self.operation = self.dialog.ui.operation_comboBox.currentText()
        self.organ = self.dialog.ui.organ_comboBox.currentText()
        self.probe_type = str(self.dialog.ui.probe_type_comboBox.currentText())
        self.probe = str(self.dialog.ui.probe_comboBox.currentText())
        self.tissue = self.dialog.ui.tissue_comboBox.currentText()
        self.tissue_wall = self.dialog.ui.tissue_wall_comboBox.currentText()
        self.calibration = str(self.dialog.ui.calibration_comboBox.currentText())
        self.integration_time = self.dialog.ui.integration_time_lineEdit.text()
        self.spectral_range = str(self.dialog.ui.spectral_range_comboBox.currentText())
################################################################################################################################
        self.classify = str(self.dialog.ui.classification_comboBox.currentText())
        self.notes = self.dialog.ui.notes_lineEdit.text()
        if self.gender == "" or self.age == "" or self.therapy == "" or self.operation == "" or self.organ == "" or self.probe_type == "" or self.tissue == "" or self.tissue_wall == "" or self.calibration == "" or self.integration_time == "" or self.spectral_range == "" or self.classify == "" or self.notes == "" or self.probe == "":
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Please fill in all parameters.")
            msg.exec_()
            return
        elif self.organ == 'GOJ' and self.classify == 'ON':
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Critical)
            msg.setWindowTitle("Critical")
            msg.setText("You can not use live classification with this type of tissue. Please either choose classification 'OFF' or another tissue type and try again.")
            msg.exec_()
        elif not is_integer(self.integration_time):
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Warning)
            msg.setWindowTitle("Warning")
            msg.setText("Spectrometer's integration time must be number not string.")
            msg.exec_()
            return
        else:
            # print(self.expCounter)
            self.integration_time = int(self.integration_time)
            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            msg.setWindowTitle("Success")
            msg.setText("Parameters successfully passed.")
            msg.exec_()
            self.expParams = True
            self.dialog.ui.exp_params_buttonBox.button(QDialogButtonBox.Ok).setDisabled(False)
            dateexp = datetime.now().strftime('%Y%m%dT%H%M%S')
            folder_exp_name = dateexp + '_' + self.operation + '_' + self.organ + '_' + self.tissue + '_' + self.tissue_wall
            self.pathtoexp = self.dir_path + '/' + folder_exp_name
            if not os.path.isdir(self.pathtoexp):
                os.makedirs(self.pathtoexp)
            if self.dialog.exec_() == QDialog.Accepted:
                if self.expCounter == 0:
######################################################################################
                    self.lightOn.setDisabled(False)
                    # print(self.gender, self.age, self.therapy, self.operation, self.organ, self.probe_type, self.tissue, self.tissue_wall, self.calibration, self.integration_time, self.spectral_range)
                    self.increaseExpCounter()
                    self.startCamera.setDisabled(False)
########################################################################################
                    if self.classify == 'ON':
                        self.classification = True
                        # print('classification', self.classification)
                        # print('classify', self.classify)
                    else:
                        self.classification = False
                    self.loadCalibration()
                elif self.expCounter > 0:
                    self.increaseExpCounter()
                    self.startCamera.setDisabled(True)
########################################################################################
                    self.tissueChanged = True
                    if self.classify == 'ON':
                        self.classification = True
                    else:
                        self.classification = False
                    self.loadCalibration()

            return

    def terminateApp(self):
        # global cap
        # cap.release()
        # self.specThread.terminate()
        self.close()


if __name__ == '__main__':
    app = QApplication([])
    window = StartWindow()  # You should delete the Camera(0) in order to use it in the main.py
    window.show()
    sys.exit(app.exec_())
