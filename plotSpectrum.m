figure(1)
plot(wavelength, meansDrs, '-o')
name = 'Oesophagus Normal Spectrum';
title(name)
xlabel('Wavelength (nm)')
ylabel('Reflectance (a.u.)')
xlim([420 720])
ylim([0 1.2])
% saveas(figure(1),strcat(name, '.png'));