import serial


def turnLightOn():
    ser = serial.Serial(
        port='COM3',
        baudrate=9600,
        timeout=1,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=False,
        rtscts=False,
        dsrdtr=False,
        writeTimeout=2
    )
    if ser.isOpen():
        ser.write('SO\r'.encode())  # SO: turn on, CO: turn off
        ser.flushInput()
        bytesToRead = ser.inWaiting()
        data = ser.read(bytesToRead)
    else:
        print("cannot open serial port ")


def turnLightOff():
    ser = serial.Serial(
        port='COM3',
        baudrate=9600,
        timeout=1,
        parity=serial.PARITY_NONE,
        stopbits=serial.STOPBITS_ONE,
        bytesize=serial.EIGHTBITS,
        xonxoff=False,
        rtscts=False,
        dsrdtr=False,
        writeTimeout=2
    )
    if ser.isOpen():
        ser.write('CO\r'.encode())  # SO: turn on, CO: turn off
        ser.flushInput()
        bytesToRead = ser.inWaiting()
        data = ser.read(bytesToRead)
    else:
        print("cannot open serial port ")

# def main():
#     turnLightOn
#     # turnLightOff

if __name__ == '__main__':
    turnLightOn()
    # turnLightOff()