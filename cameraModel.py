import cv2
from datetime import datetime


class Camera:
    def __init__(self, cam_num):
        self.cam_num = cam_num
        self.cap = None

    def start_camera(self):
        self.cap = cv2.VideoCapture(self.cam_num)  #, cv2.CAP_DSHOW)
        return self.cap

    def get_frame(self):
        self.ret, self.frame = self.cap.read()
        return self.ret, self.frame

    def set_camera_parameters(self, brightness, focus, fps):
        self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, True)
        self.cap.set(cv2.CAP_PROP_AUTOFOCUS, False)
        self.cap.set(cv2.CAP_PROP_BRIGHTNESS, brightness)
        self.cap.set(cv2.CAP_PROP_FOCUS, focus)
        self.cap.set(cv2.CAP_PROP_FPS, fps)

    def write_video(self, dir, name):
        codec = cv2.VideoWriter_fourcc('M', 'J', 'P', 'G')
        self.out_video = cv2.VideoWriter(dir + '\\' + datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + name + '.avi', codec, self.cap.get(cv2.CAP_PROP_FPS), (int(self.cap.get(3)), int(self.cap.get(4))), True)
        return self.out_video

    def stop_camera(self):
        self.cap.release()
        self.cap = None
        # cv2.destroyAllWindows()

    def __str__(self):
        return 'OpenCV Camera {}'.format(self.cam_num)


