from PyQt5.QtWidgets import (QWidget, QSlider, QVBoxLayout,
                             QLabel, QApplication, QHBoxLayout, QLineEdit, QGridLayout, QSpacerItem, QSizePolicy,
                             QPushButton)
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QFont
import sys


class Example(QWidget):

    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.brightness_value = 0
        self.focus_value = 0

        brightnessLabel = QLabel('Camera brightness')
        brightnessLabel.setMinimumWidth(80)
        brightnessLabel.setAlignment(Qt.AlignCenter)
        brightnessLabel.setFont(QFont("MS Shell Dlg 2", 10))
        focusLabel = QLabel('Camera focus')
        focusLabel.setMinimumWidth(80)
        focusLabel.setAlignment(Qt.AlignCenter)
        focusLabel.setFont(QFont("MS Shell Dlg 2", 10))

        self.brightness_sld = QSlider(Qt.Horizontal, self)
        self.brightness_sld.setRange(0, 255)
        self.brightness_sld.setFocusPolicy(Qt.NoFocus)
        self.brightness_sld.setPageStep(5)
        self.brightness_sld.setTickPosition(QSlider.TicksBelow)
        self.brightness_sld.setTickInterval(35)
        self.brightness_sld.valueChanged.connect(self.updateBrightnessLabel)

        self.brightness_label = QLabel('0', self)
        self.brightness_label.setMinimumWidth(80)
        self.brightness_label.setAlignment(Qt.AlignCenter)
        self.brightness_label.setFont(QFont("MS Shell Dlg 2", 12))

        self.focus_sld = QSlider(Qt.Horizontal, self)
        self.focus_sld.setRange(0, 40)
        self.focus_sld.setFocusPolicy(Qt.NoFocus)
        self.focus_sld.setPageStep(5)
        self.focus_sld.setTickPosition(QSlider.TicksBelow)
        self.focus_sld.setTickInterval(5)
        self.focus_sld.valueChanged.connect(self.updateFocusLabel)

        self.focus_label = QLabel('0', self)
        self.focus_label.setMinimumWidth(80)
        self.focus_label.setAlignment(Qt.AlignCenter)
        self.focus_label.setFont(QFont("MS Shell Dlg 2", 12))

        self.ok_button = QPushButton('Get Camera Parameters', self)
        # self.ok_button.clicked.connect(self.get_camera_parameters)

        self.close_button = QPushButton('Close', self)
        self.close_button.clicked.connect(self.close)
        self.close_button.setDisabled(True)

        grid = QGridLayout()
        grid.setSpacing(30)

        grid.addWidget(brightnessLabel, 0, 1)
        grid.addWidget(self.brightness_label, 0, 3)
        grid.addWidget(self.brightness_sld, 0, 2)

        grid.addWidget(focusLabel, 1, 1)
        grid.addWidget(self.focus_sld, 1, 2)
        grid.addWidget(self.focus_label, 1, 3)

        grid.addWidget(self.ok_button, 2, 1)

        grid.addWidget(self.close_button, 2, 3)

        self.setLayout(grid)

        self.setGeometry(0, 0, 480, 220)
        self.setWindowTitle('Set Camera Parameters')
        # self.show()

    def updateBrightnessLabel(self, brightness):
        self.brightness_label.setText(str(brightness))

    def updateFocusLabel(self, focus):
        self.focus_label.setText(str(focus))
