from camera import *
from cameraModel import *
from experiementParams import Ui_Dialog
from PyQt5.QtWidgets import QDialog

class ExperParamDlg(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)

app = QApplication([])
dlg = ExperParamDlg()
dlg.exec()
app.exit(app.exec_())